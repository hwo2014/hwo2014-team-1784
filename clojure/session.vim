let SessionLoad = 1
if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
inoremap <silent> <F6> :ConqueTermSendBufferInsert
inoremap <silent> <F5> :ConqueTermSendLineInsert
nnoremap <silent>  :nohlsearch
xmap S <Plug>VSurround
nmap [xx <Plug>unimpaired_line_xml_encode
xmap [x <Plug>unimpaired_xml_encode
nmap [x <Plug>unimpaired_xml_encode
nmap [uu <Plug>unimpaired_line_url_encode
xmap [u <Plug>unimpaired_url_encode
nmap [u <Plug>unimpaired_url_encode
nmap [yy <Plug>unimpaired_line_string_encode
xmap [y <Plug>unimpaired_string_encode
nmap [y <Plug>unimpaired_string_encode
nmap [p <Plug>unimpairedPutAbove
nnoremap [ov :set virtualedit+=all
nnoremap [ox :set cursorline cursorcolumn
nnoremap [ow :set wrap
nnoremap [os :set spell
nnoremap [or :set relativenumber
nnoremap [on :set number
nnoremap [ol :set list
nnoremap [oi :set ignorecase
nnoremap [oh :set hlsearch
nnoremap [od :diffthis
nnoremap [ou :set cursorcolumn
nnoremap [oc :set cursorline
nnoremap [ob :set background=light
xmap [e <Plug>unimpairedMoveSelectionUp
nmap [e <Plug>unimpairedMoveUp
nmap [  <Plug>unimpairedBlankUp
omap [n <Plug>unimpairedContextPrevious
nmap [n <Plug>unimpairedContextPrevious
nmap [o <Plug>unimpairedOPrevious
nmap [f <Plug>unimpairedDirectoryPrevious
nmap <silent> [T <Plug>unimpairedTFirst
nmap <silent> [t <Plug>unimpairedTPrevious
nmap <silent> [ <Plug>unimpairedQPFile
nmap <silent> [Q <Plug>unimpairedQFirst
nmap <silent> [q <Plug>unimpairedQPrevious
nmap <silent> [ <Plug>unimpairedLPFile
nmap <silent> [L <Plug>unimpairedLFirst
nmap <silent> [l <Plug>unimpairedLPrevious
nmap <silent> [A <Plug>unimpairedAFirst
nmap <silent> [a <Plug>unimpairedAPrevious
nnoremap <silent> [B :bfirst
nnoremap <silent> [b :bprevious
vmap [% [%m'gv``
nnoremap \f :Unite -start-insert file_rec
nmap \* :execute 'noautocmd vimgrep /\V' . substitute(escape(expand("<cword>"), '\'), '\n', '\\n', 'g') . '/ **'
map <silent> \e :GhcModCheck
nmap ]xx <Plug>unimpaired_line_xml_decode
xmap ]x <Plug>unimpaired_xml_decode
nmap ]x <Plug>unimpaired_xml_decode
nmap ]uu <Plug>unimpaired_line_url_decode
xmap ]u <Plug>unimpaired_url_decode
nmap ]u <Plug>unimpaired_url_decode
nmap ]yy <Plug>unimpaired_line_string_decode
xmap ]y <Plug>unimpaired_string_decode
nmap ]y <Plug>unimpaired_string_decode
nmap ]p <Plug>unimpairedPutBelow
nnoremap ]ov :set virtualedit-=all
nnoremap ]ox :set nocursorline nocursorcolumn
nnoremap ]ow :set nowrap
nnoremap ]os :set nospell
nnoremap ]or :set norelativenumber
nnoremap ]on :set nonumber
nnoremap ]ol :set nolist
nnoremap ]oi :set noignorecase
nnoremap ]oh :set nohlsearch
nnoremap ]od :diffoff
nnoremap ]ou :set nocursorcolumn
nnoremap ]oc :set nocursorline
nnoremap ]ob :set background=dark
xmap ]e <Plug>unimpairedMoveSelectionDown
nmap ]e <Plug>unimpairedMoveDown
nmap ]  <Plug>unimpairedBlankDown
omap ]n <Plug>unimpairedContextNext
nmap ]n <Plug>unimpairedContextNext
nmap ]o <Plug>unimpairedONext
nmap ]f <Plug>unimpairedDirectoryNext
nmap <silent> ]T <Plug>unimpairedTLast
nmap <silent> ]t <Plug>unimpairedTNext
nmap <silent> ] <Plug>unimpairedQNFile
nmap <silent> ]Q <Plug>unimpairedQLast
nmap <silent> ]q <Plug>unimpairedQNext
nmap <silent> ] <Plug>unimpairedLNFile
nmap <silent> ]L <Plug>unimpairedLLast
nmap <silent> ]l <Plug>unimpairedLNext
nmap <silent> ]A <Plug>unimpairedALast
nmap <silent> ]a <Plug>unimpairedANext
nnoremap <silent> ]B :blast
nnoremap <silent> ]b :bnext
vmap ]% ]%m'gv``
vmap a% [%v]%
nnoremap cov :set =(&virtualedit =~# "all") ? 'virtualedit-=all' : 'virtualedit+=all'
nnoremap cox :set =&cursorline && &cursorcolumn ? 'nocursorline nocursorcolumn' : 'cursorline cursorcolumn'
nnoremap cod :=&diff ? 'diffoff' : 'diffthis'
nnoremap cob :set background==&background == 'dark' ? 'light' : 'dark'
nmap cs <Plug>Csurround
nmap cr <Plug>Coerce
nmap ds <Plug>Dsurround
nmap gx <Plug>NetrwBrowseX
xmap gS <Plug>VgSurround
nmap ySS <Plug>YSsurround
nmap ySs <Plug>YSsurround
nmap yss <Plug>Yssurround
nmap yS <Plug>YSurround
nmap ys <Plug>Ysurround
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#NetrwBrowseX(expand("<cWORD>"),0)
nnoremap <silent> <F11> :call conque_term#exec_file()
nnoremap <silent> <F6> :ConqueTermSendBufferNormal
vnoremap <silent> <F5> :ConqueTermSendSelection
nnoremap <silent> <F5> :ConqueTermSendLineNormal
nnoremap <Plug>FireplaceSource :Source 
nmap <silent> <Plug>unimpairedOPrevious <Plug>unimpairedDirectoryPrevious:echohl WarningMSG|echo "[o is deprecated. Use [f"|echohl NONE
nmap <silent> <Plug>unimpairedONext <Plug>unimpairedDirectoryNext:echohl WarningMSG|echo "]o is deprecated. Use ]f"|echohl NONE
nnoremap <silent> <Plug>unimpairedTLast :exe "".(v:count ? v:count : "")."tlast"
nnoremap <silent> <Plug>unimpairedTFirst :exe "".(v:count ? v:count : "")."tfirst"
nnoremap <silent> <Plug>unimpairedTNext :exe "".(v:count ? v:count : "")."tnext"
nnoremap <silent> <Plug>unimpairedTPrevious :exe "".(v:count ? v:count : "")."tprevious"
nnoremap <silent> <Plug>unimpairedQNFile :exe "".(v:count ? v:count : "")."cnfile"zv
nnoremap <silent> <Plug>unimpairedQPFile :exe "".(v:count ? v:count : "")."cpfile"zv
nnoremap <silent> <Plug>unimpairedQLast :exe "".(v:count ? v:count : "")."clast"zv
nnoremap <silent> <Plug>unimpairedQFirst :exe "".(v:count ? v:count : "")."cfirst"zv
nnoremap <silent> <Plug>unimpairedQNext :exe "".(v:count ? v:count : "")."cnext"zv
nnoremap <silent> <Plug>unimpairedQPrevious :exe "".(v:count ? v:count : "")."cprevious"zv
nnoremap <silent> <Plug>unimpairedLNFile :exe "".(v:count ? v:count : "")."lnfile"zv
nnoremap <silent> <Plug>unimpairedLPFile :exe "".(v:count ? v:count : "")."lpfile"zv
nnoremap <silent> <Plug>unimpairedLLast :exe "".(v:count ? v:count : "")."llast"zv
nnoremap <silent> <Plug>unimpairedLFirst :exe "".(v:count ? v:count : "")."lfirst"zv
nnoremap <silent> <Plug>unimpairedLNext :exe "".(v:count ? v:count : "")."lnext"zv
nnoremap <silent> <Plug>unimpairedLPrevious :exe "".(v:count ? v:count : "")."lprevious"zv
nnoremap <silent> <Plug>unimpairedBLast :exe "".(v:count ? v:count : "")."blast"
nnoremap <silent> <Plug>unimpairedBFirst :exe "".(v:count ? v:count : "")."bfirst"
nnoremap <silent> <Plug>unimpairedBNext :exe "".(v:count ? v:count : "")."bnext"
nnoremap <silent> <Plug>unimpairedBPrevious :exe "".(v:count ? v:count : "")."bprevious"
nnoremap <silent> <Plug>unimpairedALast :exe "".(v:count ? v:count : "")."last"
nnoremap <silent> <Plug>unimpairedAFirst :exe "".(v:count ? v:count : "")."first"
nnoremap <silent> <Plug>unimpairedANext :exe "".(v:count ? v:count : "")."next"
nnoremap <silent> <Plug>unimpairedAPrevious :exe "".(v:count ? v:count : "")."previous"
nnoremap <silent> <Plug>SurroundRepeat .
imap S <Plug>ISurround
imap s <Plug>Isurround
imap  <Plug>Isurround
imap jk 
let &cpo=s:cpo_save
unlet s:cpo_save
set background=dark
set backspace=indent,eol,start
set clipboard=unnamed,unnamedplus
set expandtab
set fileencodings=ucs-bom,utf-8,latin1
set guicursor=n-v-c:block-Cursor/lCursor,ve:ver35-Cursor,o:hor50-Cursor,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor,sm:block-Cursor-blinkwait175-blinkoff150-blinkon175,a:blinkon0
set guifont=Source\ Code\ Pro\ for\ Powerline\ Light\ 10
set guioptions=aegimrLt
set helplang=en
set hidden
set hlsearch
set ignorecase
set iminsert=0
set imsearch=0
set incsearch
set iskeyword=@,48-57,_,192-255,?,-,*,!,+,/,=,<,>,.,:,$
set laststatus=2
set lispwords=as->,binding,bound-fn,case,catch,cond->,cond->>,condp,def,definline,definterface,defmacro,defmethod,defmulti,defn,defn-,defonce,defprotocol,defrecord,defstruct,deftest,deftest-,deftype,doseq,dotimes,doto,extend,extend-protocol,extend-type,fn,for,if,if-let,if-not,if-some,let,letfn,locking,loop,ns,proxy,reify,set-test,testing,when,when-first,when-let,when-not,when-some,while,with-bindings,with-in-str,with-local-vars,with-open,with-precision,with-redefs,with-redefs-fn,with-test
set mouse=a
set operatorfunc=PareditChange
set redrawtime=100
set ruler
set runtimepath=~/.vim,~/.vim/bundle/vim-visual-star-search/,~/.vim/bundle/vim-abolish/,~/.vim/bundle/vim-surround/,~/.vim/bundle/vim-unimpaired/,~/.vim/bundle/vim-fugitive/,~/.vim/bundle/vim-clojure-static/,~/.vim/bundle/vim-fireplace/,~/.vim/bundle/rainbow_parentheses.vim/,~/.vim/bundle/vim-airline/,~/.vim/bundle/vim-colors-solarized/,~/.vim/bundle/vim-conque-repl/,~/.vim/bundle/conque-term/,~/.vim/bundle/vimproc/,~/.vim/bundle/ghcmod-vim/,~/.vim/bundle/unite.vim/,~/.vim/bundle/paredit.vim/,~/.vim/bundle/.neobundle,/usr/share/vim/vimfiles,/usr/share/vim/vim74,/usr/share/vim/vimfiles/after,~/.vim/after,~/.vim/bundle/neobundle.vim/,~/.vim/bundle/ghcmod-vim//after
set shiftwidth=2
set showcmd
set smartcase
set smarttab
set tabstop=2
set termencoding=utf-8
set wildmenu
set window=53
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
cd ~/Dev/hwo2014-team-1784/clojure
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +1 src/hwo2014bot/core.clj
badd +38 ~/Dev/hwo2014-team-1784/data/constant_6.txt
badd +67 ~/Dev/hwo2014-team-1784/data/constant_7.txt
badd +27 situation.clj
badd +3 src/hwo2014bot/stuff.clj
badd +6 src/hwo2014bot/core_test.clj
badd +0 ~/.lein/profiles.clj
silent! argdel *
edit src/hwo2014bot/core.clj
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
exe 'vert 1resize ' . ((&columns * 118 + 117) / 235)
exe 'vert 2resize ' . ((&columns * 116 + 117) / 235)
argglobal
let s:cpo_save=&cpo
set cpo&vim
inoremap <buffer> <expr> <Del> PareditDel()
inoremap <buffer> <expr> <BS> PareditBackspace(0)
nmap <buffer> gd <Plug>FireplaceDtabjump
nmap <buffer> d <Plug>FireplaceDsplit
nmap <buffer>  <Plug>FireplaceDsplit
vnoremap <buffer> <silent> ( :call PareditFindOpening('(',')',1)
nnoremap <buffer> <silent> ( :call PareditFindOpening('(',')',0)
vnoremap <buffer> <silent> ) :call PareditFindClosing('(',')',1)
nnoremap <buffer> <silent> ) :call PareditFindClosing('(',')',0)
nnoremap <buffer> <silent> ,S :call PareditSplice()|silent! call repeat#set(",S")
vnoremap <buffer> <silent> ,W :call PareditWrapSelection("(",")")
nnoremap <buffer> <silent> ,W :call PareditWrap("(",")")|silent! call repeat#set(",W")
nnoremap <buffer> <silent> ,J :call PareditJoin()|silent! call repeat#set(",J")
nnoremap <buffer> <silent> ,O :call PareditSplit()|silent! call repeat#set(",O")
nnoremap <buffer> <silent> ,> :call PareditMoveRight()|silent! call repeat#set(",>")
nnoremap <buffer> <silent> ,< :call PareditMoveLeft()|silent! call repeat#set(",\<")
vnoremap <buffer> <silent> ,w{ :call PareditWrapSelection("{","}")
nnoremap <buffer> <silent> ,w{ :call PareditWrap("{","}")|silent! call repeat#set(",w{")
vnoremap <buffer> <silent> ,w[ :call PareditWrapSelection("[","]")
nnoremap <buffer> <silent> ,w[ :call PareditWrap("[","]")|silent! call repeat#set(",w[")
nnoremap <buffer> <silent> ,I :call PareditRaise()|silent! call repeat#set(",I")
nmap <buffer> <silent> ,<Down> d])%,S
nmap <buffer> <silent> ,<Up> d[(,S
vnoremap <buffer> <silent> ,w" :call PareditWrapSelection('"','"')
nnoremap <buffer> <silent> ,w" :call PareditWrap('"','"')|silent! call repeat#set(",w\"")
vnoremap <buffer> <silent> ,w( :call PareditWrapSelection("(",")")
nnoremap <buffer> <silent> ,w( :call PareditWrap("(",")")|silent! call repeat#set(",w(")
nnoremap <buffer> <silent> C v$:call PareditChange(visualmode(),1)
nnoremap <buffer> <silent> D v$:call PareditDelete(visualmode(),1)|silent! call repeat#set("D")
nmap <buffer> K <Plug>FireplaceK
nnoremap <buffer> <silent> P :call PareditPut("P")|silent! call repeat#set("P")
nnoremap <buffer> <silent> S V:call PareditChange(visualmode(),1)
nnoremap <buffer> <silent> X :call PareditEraseBck()|silent! call repeat#set("X")
nnoremap <buffer> <silent> [[ :call PareditFindDefunBck()
nmap <buffer> [d <Plug>FireplaceSource
nmap <buffer> [ <Plug>FireplaceDjump
nnoremap <buffer> <silent> ]] :call PareditFindDefunFwd()
nmap <buffer> ]d <Plug>FireplaceSource
nmap <buffer> ] <Plug>FireplaceDjump
nnoremap <buffer> <silent> caw :call PareditChangeSpec('caw',1)
nnoremap <buffer> <silent> ciw :call PareditChangeSpec('ciw',1)
nnoremap <buffer> <silent> cb :call PareditChangeSpec('cb',0)
nnoremap <buffer> <silent> cW :set opfunc=PareditChangeg@E
nnoremap <buffer> <silent> cw :call PareditChangeSpec('cw',1)
nnoremap <buffer> <silent> cc :call PareditChangeLines()
vnoremap <buffer> <silent> c :call PareditChange(visualmode(),1)
nnoremap <buffer> <silent> c :set opfunc=PareditChangeg@
nnoremap <buffer> <silent> cpr :Require
nmap <buffer> cqc <Plug>FireplacePrompti
nmap <buffer> cqp <Plug>FireplacePrompt
nmap <buffer> cqq <Plug>FireplaceCountEdit
nmap <buffer> cq <Plug>FireplaceEdit
nmap <buffer> c1mm <Plug>FireplaceCount1MacroExpand
nmap <buffer> c1m <Plug>Fireplace1MacroExpand
nmap <buffer> cmm <Plug>FireplaceCountMacroExpand
nmap <buffer> cm <Plug>FireplaceMacroExpand
nmap <buffer> c!! <Plug>FireplaceCountFilter
nmap <buffer> c! <Plug>FireplaceFilter
nmap <buffer> cpp <Plug>FireplaceCountPrint
nmap <buffer> cp <Plug>FireplacePrint
nnoremap <buffer> <silent> dd :call PareditDeleteLines()|silent! call repeat#set("dd")
vnoremap <buffer> <silent> d :call PareditDelete(visualmode(),1)
nnoremap <buffer> <silent> d :call PareditSetDelete(v:count)g@
nnoremap <buffer> <silent> p :call PareditPut("p")|silent! call repeat#set("p")
nnoremap <buffer> <silent> s :call PareditEraseFwd()i
vnoremap <buffer> <silent> x :call PareditDelete(visualmode(),1)
nnoremap <buffer> <silent> x :call PareditEraseFwd()|silent! call repeat#set("x")
vnoremap <buffer> <silent> <Del> :call PareditDelete(visualmode(),1)
nnoremap <buffer> <silent> <Del> :call PareditEraseFwd()
inoremap <buffer> <expr>  PareditEnter()
map! <buffer> ( <Plug>FireplaceRecall
inoremap <buffer> <expr> " PareditInsertQuotes()
inoremap <buffer> <expr> ( PareditInsertOpening('(',')')
inoremap <buffer> <silent> ) =(pumvisible() ? "\<C-Y>" : ""):let save_ve=&ve:set ve=all:call PareditInsertClosing('(',')'):let &ve=save_ve
inoremap <buffer> <expr> [ PareditInsertOpening('[',']')
inoremap <buffer> <silent> ] =(pumvisible() ? "\<C-Y>" : ""):let save_ve=&ve:set ve=all:call PareditInsertClosing('[',']'):let &ve=save_ve
inoremap <buffer> <expr> { PareditInsertOpening('{','}')
inoremap <buffer> <silent> } =(pumvisible() ? "\<C-Y>" : ""):let save_ve=&ve:set ve=all:call PareditInsertClosing('{','}'):let &ve=save_ve
let &cpo=s:cpo_save
unlet s:cpo_save
setlocal keymap=
setlocal noarabic
setlocal noautoindent
setlocal balloonexpr=
setlocal nobinary
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
set colorcolumn=110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255
setlocal colorcolumn=110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255
setlocal comments=n:;
setlocal commentstring=;\ %s
setlocal complete=.,w,b,u,t,i
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=clojurecomplete#Complete
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal define=^\\s*(def\\w*
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=%+G,fireplace
setlocal expandtab
if &filetype != 'clojure'
setlocal filetype=clojure
endif
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=0
setlocal foldmarker={{{,}}}
setlocal foldmethod=manual
setlocal foldminlines=1
setlocal foldnestmax=20
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=cq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=0
setlocal include=
setlocal includeexpr=tr(v:fname,'.-','/_')
setlocal indentexpr=GetClojureIndent()
setlocal indentkeys=!,o,O
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255,?,-,*,!,+,/,=,<,>,.,:,$
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal nolist
setlocal makeprg=lein
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=octal,hex
set number
setlocal number
setlocal numberwidth=4
setlocal omnifunc=fireplace#omnicomplete
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal shiftwidth=2
setlocal noshortname
setlocal nosmartindent
setlocal softtabstop=2
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=
setlocal spelllang=en
setlocal statusline=%!airline#statusline(1)
setlocal suffixesadd=.clj,.cljx,.cljs,.java
setlocal swapfile
setlocal synmaxcol=3000
if &syntax != 'clojure'
setlocal syntax=clojure
endif
setlocal tabstop=2
setlocal tags=~/Dev/hwo2014-team-1784/.git/clojure.tags,~/Dev/hwo2014-team-1784/.git/tags,./tags,./TAGS,tags,TAGS
setlocal textwidth=0
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal nowinfixheight
setlocal nowinfixwidth
set nowrap
setlocal nowrap
setlocal wrapmargin=0
silent! normal! zE
let s:l = 56 - ((41 * winheight(0) + 26) / 52)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
56
normal! 030|
wincmd w
argglobal
edit src/hwo2014bot/core_test.clj
let s:cpo_save=&cpo
set cpo&vim
inoremap <buffer> <expr> <Del> PareditDel()
inoremap <buffer> <expr> <BS> PareditBackspace(0)
nmap <buffer> gd <Plug>FireplaceDtabjump
nmap <buffer> d <Plug>FireplaceDsplit
nmap <buffer>  <Plug>FireplaceDsplit
vnoremap <buffer> <silent> ( :call PareditFindOpening('(',')',1)
nnoremap <buffer> <silent> ( :call PareditFindOpening('(',')',0)
vnoremap <buffer> <silent> ) :call PareditFindClosing('(',')',1)
nnoremap <buffer> <silent> ) :call PareditFindClosing('(',')',0)
nnoremap <buffer> <silent> ,S :call PareditSplice()|silent! call repeat#set(",S")
vnoremap <buffer> <silent> ,W :call PareditWrapSelection("(",")")
nnoremap <buffer> <silent> ,W :call PareditWrap("(",")")|silent! call repeat#set(",W")
nnoremap <buffer> <silent> ,J :call PareditJoin()|silent! call repeat#set(",J")
nnoremap <buffer> <silent> ,O :call PareditSplit()|silent! call repeat#set(",O")
nnoremap <buffer> <silent> ,> :call PareditMoveRight()|silent! call repeat#set(",>")
nnoremap <buffer> <silent> ,< :call PareditMoveLeft()|silent! call repeat#set(",\<")
vnoremap <buffer> <silent> ,w{ :call PareditWrapSelection("{","}")
nnoremap <buffer> <silent> ,w{ :call PareditWrap("{","}")|silent! call repeat#set(",w{")
vnoremap <buffer> <silent> ,w[ :call PareditWrapSelection("[","]")
nnoremap <buffer> <silent> ,w[ :call PareditWrap("[","]")|silent! call repeat#set(",w[")
nnoremap <buffer> <silent> ,I :call PareditRaise()|silent! call repeat#set(",I")
nmap <buffer> <silent> ,<Down> d])%,S
nmap <buffer> <silent> ,<Up> d[(,S
vnoremap <buffer> <silent> ,w" :call PareditWrapSelection('"','"')
nnoremap <buffer> <silent> ,w" :call PareditWrap('"','"')|silent! call repeat#set(",w\"")
vnoremap <buffer> <silent> ,w( :call PareditWrapSelection("(",")")
nnoremap <buffer> <silent> ,w( :call PareditWrap("(",")")|silent! call repeat#set(",w(")
nnoremap <buffer> <silent> C v$:call PareditChange(visualmode(),1)
nnoremap <buffer> <silent> D v$:call PareditDelete(visualmode(),1)|silent! call repeat#set("D")
nmap <buffer> K <Plug>FireplaceK
nnoremap <buffer> <silent> P :call PareditPut("P")|silent! call repeat#set("P")
nnoremap <buffer> <silent> S V:call PareditChange(visualmode(),1)
nnoremap <buffer> <silent> X :call PareditEraseBck()|silent! call repeat#set("X")
nnoremap <buffer> <silent> [[ :call PareditFindDefunBck()
nmap <buffer> [d <Plug>FireplaceSource
nmap <buffer> [ <Plug>FireplaceDjump
nnoremap <buffer> <silent> ]] :call PareditFindDefunFwd()
nmap <buffer> ]d <Plug>FireplaceSource
nmap <buffer> ] <Plug>FireplaceDjump
nnoremap <buffer> <silent> caw :call PareditChangeSpec('caw',1)
nnoremap <buffer> <silent> ciw :call PareditChangeSpec('ciw',1)
nnoremap <buffer> <silent> cb :call PareditChangeSpec('cb',0)
nnoremap <buffer> <silent> cW :set opfunc=PareditChangeg@E
nnoremap <buffer> <silent> cw :call PareditChangeSpec('cw',1)
nnoremap <buffer> <silent> cc :call PareditChangeLines()
vnoremap <buffer> <silent> c :call PareditChange(visualmode(),1)
nnoremap <buffer> <silent> c :set opfunc=PareditChangeg@
nnoremap <buffer> <silent> cpr :Require
nmap <buffer> cqc <Plug>FireplacePrompti
nmap <buffer> cqp <Plug>FireplacePrompt
nmap <buffer> cqq <Plug>FireplaceCountEdit
nmap <buffer> cq <Plug>FireplaceEdit
nmap <buffer> c1mm <Plug>FireplaceCount1MacroExpand
nmap <buffer> c1m <Plug>Fireplace1MacroExpand
nmap <buffer> cmm <Plug>FireplaceCountMacroExpand
nmap <buffer> cm <Plug>FireplaceMacroExpand
nmap <buffer> c!! <Plug>FireplaceCountFilter
nmap <buffer> c! <Plug>FireplaceFilter
nmap <buffer> cpp <Plug>FireplaceCountPrint
nmap <buffer> cp <Plug>FireplacePrint
nnoremap <buffer> <silent> dd :call PareditDeleteLines()|silent! call repeat#set("dd")
vnoremap <buffer> <silent> d :call PareditDelete(visualmode(),1)
nnoremap <buffer> <silent> d :call PareditSetDelete(v:count)g@
nnoremap <buffer> <silent> p :call PareditPut("p")|silent! call repeat#set("p")
nnoremap <buffer> <silent> s :call PareditEraseFwd()i
vnoremap <buffer> <silent> x :call PareditDelete(visualmode(),1)
nnoremap <buffer> <silent> x :call PareditEraseFwd()|silent! call repeat#set("x")
vnoremap <buffer> <silent> <Del> :call PareditDelete(visualmode(),1)
nnoremap <buffer> <silent> <Del> :call PareditEraseFwd()
inoremap <buffer> <expr>  PareditEnter()
map! <buffer> ( <Plug>FireplaceRecall
inoremap <buffer> <expr> " PareditInsertQuotes()
inoremap <buffer> <expr> ( PareditInsertOpening('(',')')
inoremap <buffer> <silent> ) =(pumvisible() ? "\<C-Y>" : ""):let save_ve=&ve:set ve=all:call PareditInsertClosing('(',')'):let &ve=save_ve
inoremap <buffer> <expr> [ PareditInsertOpening('[',']')
inoremap <buffer> <silent> ] =(pumvisible() ? "\<C-Y>" : ""):let save_ve=&ve:set ve=all:call PareditInsertClosing('[',']'):let &ve=save_ve
inoremap <buffer> <expr> { PareditInsertOpening('{','}')
inoremap <buffer> <silent> } =(pumvisible() ? "\<C-Y>" : ""):let save_ve=&ve:set ve=all:call PareditInsertClosing('{','}'):let &ve=save_ve
let &cpo=s:cpo_save
unlet s:cpo_save
setlocal keymap=
setlocal noarabic
setlocal noautoindent
setlocal balloonexpr=
setlocal nobinary
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
set colorcolumn=110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255
setlocal colorcolumn=110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255
setlocal comments=n:;
setlocal commentstring=;\ %s
setlocal complete=.,w,b,u,t,i
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=clojurecomplete#Complete
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal define=^\\s*(def\\w*
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=%+G,fireplace
setlocal expandtab
if &filetype != 'clojure'
setlocal filetype=clojure
endif
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=0
setlocal foldmarker={{{,}}}
setlocal foldmethod=manual
setlocal foldminlines=1
setlocal foldnestmax=20
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=cq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=0
setlocal include=
setlocal includeexpr=tr(v:fname,'.-','/_')
setlocal indentexpr=GetClojureIndent()
setlocal indentkeys=!,o,O
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255,?,-,*,!,+,/,=,<,>,.,:,$
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal nolist
setlocal makeprg=lein
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=octal,hex
set number
setlocal number
setlocal numberwidth=4
setlocal omnifunc=fireplace#omnicomplete
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal shiftwidth=2
setlocal noshortname
setlocal nosmartindent
setlocal softtabstop=2
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=
setlocal spelllang=en
setlocal statusline=%!airline#statusline(2)
setlocal suffixesadd=.clj,.cljx,.cljs,.java
setlocal swapfile
setlocal synmaxcol=3000
if &syntax != 'clojure'
setlocal syntax=clojure
endif
setlocal tabstop=2
setlocal tags=~/Dev/hwo2014-team-1784/.git/clojure.tags,~/Dev/hwo2014-team-1784/.git/tags,./tags,./TAGS,tags,TAGS
setlocal textwidth=0
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal nowinfixheight
setlocal nowinfixwidth
set nowrap
setlocal nowrap
setlocal wrapmargin=0
silent! normal! zE
let s:l = 90 - ((46 * winheight(0) + 26) / 52)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
90
normal! 072|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 118 + 117) / 235)
exe 'vert 2resize ' . ((&columns * 116 + 117) / 235)
tabnext 1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
