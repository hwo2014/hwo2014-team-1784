; situation structure

(def a-situation
  {:your-car {:name "un nom"
              :color "red"}
   :pieces [{:lenght 100 :lanes-length [100 100]}
            {:length 157.08 :radius 200 :angle 45 :lanes-length [164.934 149.226]}
            {:length 150 :lanes-length [150 150] :switch true}
            {:length 73.304 :radius 140 :angle 30 :lanes-length [78.540 68.068] :switch true}]
   :lanes [-10 10]
   :cars {"red" {:dimensions {:length 40.0
                             :width 20.0
                             :guideFlagPosition 10.0}
                 :angle 0.0
                 :pieceIndex 0
                 :inPieceDistance 0.0
                 :previousIndex 0
                 :previousInPieceDistance 0.0
                 :speed 5.234
                 :previous-speed 6.785
                 :lane {:startLaneIndex 0
                        :endLaneIndex 0}
                 :lap 0 }
          "green" {:dimensions {:length 40.0
                                :width 20.0
                                :guideFlagPosition 10.0}
                   :angle 0.0
                   :piecePosition
                   :pieceIndex 0
                   :previousIndex 0
                   :previousInPieceDistance 0.0
                   :inPieceDistance 0.0
                   :speed 4.562
                   :previous-speed 6.852
                   :lane {:startLaneIndex 0
                          :endLaneIndex 0}
                   :lap 0}}
   :current-msgType "carPosition"
   :game-tick 0
   :game-started true})
