(ns hwo2014bot.core-test
  (:require [clojure.test :refer :all]
            )
  (:use [hwo2014bot.core]
        [hwo2014bot.measure]
        [hwo2014bot.dataset]))

(deftest handle-msg-test
  (testing "handle 'yourCar' msg"
    (def msg {:msgType "yourCar" :data {:name "toto" :color "red"}})
    (def situation (handle-msg msg nil))
    (is (= (:your-car situation) (:data msg))))

  (testing "handle 'gameInit' msg"
    (is (= (handle-msg ginit nil) sit-ini)) )

  (testing "handle 'carPosition' msg"
    (is (= (handle-msg carpo sit-ini) tick-0))
    (is (= (handle-msg carpo-1 tick-0) tick-1)))
  
  (testing "handle 'crash' msg"
    (is (= (handle-msg crash tick-1) tick-crash)))
  )

(deftest speed-per-tick-test
  (is (= (speed-per-tick nil nil 0 0 tick-0) 0))
  (is (= (speed-per-tick nil nil nil 0 tick-0) 0))
  (is (= (speed-per-tick nil 0 0 0 tick-0) 0))
  (is (= (speed-per-tick 0 1 0 1 tick-0) 0))
  (is (= (speed-per-tick 1 0 1 1.345 tick-0) 1.345))
  (is (= (speed-per-tick 1 95 2 1.345 tick-0) 6.345)))
