(ns hwo2014bot.measure)

; piece-lane-length: piece lane -> length
(defn piece-lane-length [piece distance-from-center]
  (cond
    (:length piece) (:length piece)
    (:angle piece) (let [rad (Math/toRadians (:angle piece))
                         radius (:radius piece)]
                     (if (pos? rad)
                       (* (- radius distance-from-center) rad)
                       (* (+ radius distance-from-center) (- rad))))))

(defn lane-length [pieces distance-from-center]
  (reduce #(+ % (piece-lane-length %2 distance-from-center))
          0
          pieces))

(defn speed-per-tick [previous-pieceIndex previous-inPieceDistance pieceIndex inPieceDistance color situation]
    (cond
      (some nil? [previous-pieceIndex previous-inPieceDistance])
      0
      (= [previous-pieceIndex previous-inPieceDistance] [pieceIndex inPieceDistance])
      0
      (not= previous-pieceIndex  pieceIndex)
      (get-in situation [:cars color :speed])
      :otherwise
      (- inPieceDistance previous-inPieceDistance)))

(defn acceleration [v0 v1]
  (if (zero? v0)
    0
    (/ v1 v0)))
