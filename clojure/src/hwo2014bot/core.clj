(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [clojure.pprint]
            )
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]]
        [clojure.pprint :only [pprint]]
        [hwo2014bot.measure]
        )
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

; msg situation -> situation
(defmulti handle-msg (fn [msg situation] (:msgType msg)))

(defmethod handle-msg "yourCar" [msg situation]
  (assoc situation :your-car (:data msg)))

(defmethod handle-msg "gameInit" [msg situation]
  (let [pieces (get-in msg [:data :race :track :pieces])
        lanes  (get-in msg [:data :race :track :lanes])
        cars   (get-in msg [:data :race :cars])
        race-session (:raceSession (:race (:data msg)))]
    (assoc situation
           :current-msgType "gameInit"
           :pieces (into [] (map (fn [p] (if (nil? (:length p))
                                  (assoc p :length (piece-lane-length p 10))
                                  p))
                        pieces))
           :lanes  (vec (for [e (sort-by #(:index %) lanes)]
                          (:distanceFromCenter e)))
           :race-session race-session
           :cars (reduce #(assoc %1
                                 (get-in %2 [:id :color])
                                 (assoc {} :dimensions (:dimensions %2)))
                         {}
                         cars))))

(defmethod handle-msg "gameStart" [msg situation]
  (assoc situation :game-started true))

(defmethod handle-msg "carPositions" [msg situation]
  (let [current-cars (:data msg)
        previous-pieceIndex (fn [color]
                              (get-in situation [:cars color :pieceIndex]))
        previous-inPieceDistance (fn [color]
                                   (get-in situation [:cars color :inPieceDistance]))
        previous-speed (fn [color]
                         (get-in situation [:cars color :speed]))
        speed (fn [color cc]
               (speed-per-tick (previous-pieceIndex color) (previous-inPieceDistance color)
                               (get-in cc [:piecePosition :pieceIndex])
                               (get-in cc [:piecePosition :inPieceDistance])
                               color
                               situation))
        format-car (fn [cc] (let [piece-position (select-keys (:piecePosition cc) (keys (:piecePosition cc)))
                                  color (get-in cc [:id :color])]
                              (assoc {} color
                                     (merge piece-position
                                            (select-keys cc [:angle])
                                            {:speed (speed color cc)}
                                            {:previousSpeed (previous-speed color)}
                                            {:previousPieceIndex (previous-pieceIndex color)}
                                            {:previousInPieceDistance (previous-inPieceDistance color)}))))]
    (assoc situation
           :cars (apply merge (map format-car current-cars))
           :current-msgType "carPositions"
           :game-tick (:gameTick msg))))

(defmethod handle-msg "crash" [msg situation]
  (let [crashed-car (get-in msg [:data :color])
        crash-count (get-in situation [:cars crashed-car :crash-count])
        formated-car (assoc (get-in situation [:cars crashed-car])
                          :crashed true
                          :crashTick (:gameTick msg)
                          :crash-count (if (nil? crash-count)
                                         0
                                         (inc crash-count)))]
    (assoc-in situation [:cars crashed-car] formated-car)))

(defmethod handle-msg :default [msg situation]
  situation)

(defn decide [situation]
  (cond
    (:game-started situation) {:msgType "throttle" :data 1.0}
    :otherwise {:msgType "ping" :data "ping"}))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (str "Joined" \newline)
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn log-all-msg [msg]
  (clojure.pprint/pprint msg))

(defn game-loop [channel situation]
  (let [msg (read-message channel)]
    ;(log-all-msg msg)
    (let [current-situation (handle-msg msg situation)
          decision (decide current-situation)]
      (send-message channel decision)
      ;(log-all-msg decision)
      (println (:game-tick situation) " "
               (get-in situation [:cars "red" :speed]) " "
               (get-in situation [:cars "red" :angle]) " "
               (get-in situation [:cars "red" :pieceIndex] " ")
           
           )
      (recur channel current-situation))))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel nil)))
