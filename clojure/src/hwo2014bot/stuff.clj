(ns hwo2014bot.stuff)

(def ginit {:msgType "gameInit",
            :data {:race {
                          :track {:id "keimola",
                                  :name "Keimola",
                                  :pieces [{:length 100.0}
                                           {:length 100.0}
                                           {:length 100.0}
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 200, :angle 22.5, :switch true}
                                           {:length 100.0} {:length 100.0}
                                           {:radius 200, :angle -22.5}
                                           {:length 100.0}
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle -45.0}
                                           {:radius 100, :angle -45.0}
                                           {:radius 100, :angle -45.0}
                                           {:radius 100, :angle -45.0}
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 200, :angle 22.5}
                                           {:radius 200, :angle -22.5}
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:length 62.0}
                                           {:radius 100, :angle -45.0, :switch true}
                                           {:radius 100, :angle -45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:length 100.0, :switch true}
                                           {:length 100.0}
                                           {:length 100.0}
                                           {:length 100.0}
                                           {:length 90.0}],
                                  :lanes [{:distanceFromCenter -10, :index 0}
                                          {:distanceFromCenter 10, :index 1}],
                                  :startingPoint {:position {:x -300.0, :y -44.0}, :angle 90.0}},
                          :cars [{:id {:name "Levodore", :color "red"},
                                  :dimensions {:length 40.0, :width 20.0, :guideFlagPosition 10.0}}
                                 {:id {name "toto" :color "green"}
                                  :dimensions {:length 40.0 :width 20.0 :guideFlagPosition 10.0}}],
                          :raceSession {:laps 3, :maxLapTimeMs 60000, :quickRace true}}}, :gameId "7446f4e2-4519-4df8-b5e0-a4e45ea76ce7"})

(def carpo {:msgType "carPositions",
            :data
            [{:id {:name "Levodore", :color "red"},
              :angle 0.0,
              :piecePosition
              {:pieceIndex 0,
               :inPieceDistance 0.0,
               :lane {:startLaneIndex 0, :endLaneIndex 0},
               :lap 0}}
             {:id {:name "toto" :color "green"}
              :angle 0.0
              :piecePosition {:pieceIndex 0
                              :inPieceDistance 0.0
                              :lane {:startLaneIndex 1, :endLaneIndex 1}
                              :lap 0}}],
            :gameId "8dbbeeaa-cb0f-4b9d-82dc-e52a029c4dd7",
            :gameTick 2})
