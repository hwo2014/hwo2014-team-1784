(ns hwo2014bot.dataset
  (:use [hwo2014bot.core])
  )

(def ginit {:msgType "gameInit",
            :data {:race {
                          :track {:id "keimola",
                                  :name "Keimola",
                                  :pieces [{:length 100.0}
                                           {:length 100.0}
                                           {:length 100.0}
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle 45.0} ;4
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 200, :angle 22.5, :switch true} ;8
                                           {:length 100.0}
                                           {:length 100.0}
                                           {:radius 200, :angle -22.5} ;11
                                           {:length 100.0}
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle -45.0} ;14
                                           {:radius 100, :angle -45.0}
                                           {:radius 100, :angle -45.0}
                                           {:radius 100, :angle -45.0} ;17
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle 45.0} ;19
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 200, :angle 22.5} ;23
                                           {:radius 200, :angle -22.5} ;24
                                           {:length 100.0, :switch true}
                                           {:radius 100, :angle 45.0} ;26
                                           {:radius 100, :angle 45.0} ;27
                                           {:length 62.0}
                                           {:radius 100, :angle -45.0, :switch true} ;29
                                           {:radius 100, :angle -45.0}
                                           {:radius 100, :angle 45.0} ;31
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0}
                                           {:radius 100, :angle 45.0} ;34
                                           {:length 100.0, :switch true}
                                           {:length 100.0}
                                           {:length 100.0}
                                           {:length 100.0}
                                           {:length 90.0}],
                                  :lanes [{:distanceFromCenter -10, :index 0}
                                          {:distanceFromCenter 10, :index 1}],
                                  :startingPoint {:position {:x -300.0, :y -44.0}, :angle 90.0}},
                          :cars [{:id {:name "Levodore", :color "red"},
                                  :dimensions {:length 40.0, :width 20.0, :guideFlagPosition 10.0}}
                                 {:id {:name "toto" :color "green"}
                                  :dimensions {:length 40.0 :width 20.0 :guideFlagPosition 10.0}}],
                          :raceSession {:laps 3, :maxLapTimeMs 60000, :quickRace true}}},
            :gameId "7446f4e2-4519-4df8-b5e0-a4e45ea76ce7"})

(def carpo {:msgType "carPositions",
            :data
            [{:id {:name "Levodore", :color "red"},
              :angle 0.0,
              :piecePosition
              {:pieceIndex 0,
               :inPieceDistance 0.0,
               :lane {:startLaneIndex 0, :endLaneIndex 0},
               :lap 0}}
             {:id {:name "toto" :color "green"}
              :angle 0.0
              :piecePosition {:pieceIndex 0
                              :inPieceDistance 0.0
                              :lane {:startLaneIndex 1, :endLaneIndex 1}
                              :lap 0}}],
            :gameId "8dbbeeaa-cb0f-4b9d-82dc-e52a029c4dd7",
            :gameTick 0})

(def carpo-1 {:msgType "carPositions",
              :data
              [{:id {:name "Levodore", :color "red"},
                :angle 0.0,
                :piecePosition
                {:pieceIndex 0,
                 :inPieceDistance 2.0,
                 :lane {:startLaneIndex 0, :endLaneIndex 0},
                 :lap 0}}
               {:id {:name "toto" :color "green"}
                :angle 0.0
                :piecePosition {:pieceIndex 0
                                :inPieceDistance 1.0
                                :lane {:startLaneIndex 1, :endLaneIndex 1}
                                :lap 0}}],
              :gameId "8dbbeeaa-cb0f-4b9d-82dc-e52a029c4dd7",
              :gameTick 0})

(def crash {:msgType "crash"
            :data {:name "Levodore"
                   :color "red"}
            :gameId "uiorewhf"
            :gameTick 10})

(def sit-ini {:cars {"green" {:dimensions {:width 20.0, :length 40.0, :guideFlagPosition 10.0}},
                     "red" {:dimensions {:width 20.0, :length 40.0, :guideFlagPosition 10.0}}},
              :race-session {:quickRace true, :laps 3, :maxLapTimeMs 60000}, :lanes [-10 10],
              :pieces [{:length 100.0}
                       {:length 100.0}
                       {:length 100.0}
                       {:switch true, :length 100.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 200, :switch true, :length 78.53981633974483, :angle 22.5}
                       {:length 100.0}
                       {:length 100.0}
                       {:radius 200, :length 78.53981633974483, :angle -22.5}
                       {:length 100.0}
                       {:switch true, :length 100.0}
                       {:radius 100, :length 78.53981633974483, :angle -45.0}
                       {:radius 100, :length 78.53981633974483, :angle -45.0}
                       {:radius 100, :length 78.53981633974483, :angle -45.0}
                       {:radius 100, :length 78.53981633974483, :angle -45.0}
                       {:switch true, :length 100.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 200, :length 78.53981633974483, :angle 22.5} {:radius 200, :length 78.53981633974483, :angle -22.5}
                       {:switch true, :length 100.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:length 62.0}
                       {:radius 100, :switch true, :length 78.53981633974483, :angle -45.0} {:radius 100, :length 78.53981633974483, :angle -45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:radius 100, :length 78.53981633974483, :angle 45.0}
                       {:switch true, :length 100.0}
                       {:length 100.0}
                       {:length 100.0}
                       {:length 100.0}
                       {:length 90.0}], :current-msgType "gameInit"})
(def tick-0 {:game-tick 0,
             :pieces [{:length 100.0}
                      {:length 100.0}
                      {:length 100.0}
                      {:switch true, :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 200, :switch true, :length 78.53981633974483, :angle 22.5}
                      {:length 100.0}
                      {:length 100.0}
                      {:radius 200, :length 78.53981633974483, :angle -22.5}
                      {:length 100.0}
                      {:switch true, :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:switch true, :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 200, :length 78.53981633974483, :angle 22.5}
                      {:radius 200, :length 78.53981633974483, :angle -22.5}
                      {:switch true, :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:length 62.0}
                      {:radius 100, :switch true, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:switch true, :length 100.0}
                      {:length 100.0}
                      {:length 100.0}
                      {:length 100.0}
                      {:length 90.0}],
             :lanes [-10 10],
             :current-msgType "carPositions", 
             :cars {"green" {:previousInPieceDistance nil, 
                             :previousPieceIndex nil, 
                             :speed 0,
                             :previousSpeed nil,
                             :angle 0.0, 
                             :lap 0, 
                             :pieceIndex 0, 
                             :lane {:endLaneIndex 1, 
                                    :startLaneIndex 1}, 
                             :inPieceDistance 0.0},
                    "red" {:previousInPieceDistance nil,
                           :previousPieceIndex nil,
                           :speed 0,
                           :previousSpeed nil,
                           :angle 0.0,
                           :lap 0,
                           :pieceIndex 0,
                           :lane {:endLaneIndex 0, :startLaneIndex 0},
                           :inPieceDistance 0.0}},
             :race-session {:quickRace true, :laps 3, :maxLapTimeMs 60000}})

(def tick-1 {:game-tick 0,
             :pieces [{:length 100.0}
                      {:length 100.0}
                      {:length 100.0}
                      {:switch true
                       :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 200, :switch true, :length 78.53981633974483, :angle 22.5}
                      {:length 100.0}
                      {:length 100.0}
                      {:radius 200, :length 78.53981633974483, :angle -22.5}
                      {:length 100.0}
                      {:switch true, :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:switch true, :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 200, :length 78.53981633974483, :angle 22.5}
                      {:radius 200, :length 78.53981633974483, :angle -22.5}
                      {:switch true, :length 100.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:length 62.0}
                      {:radius 100, :switch true, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle -45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:radius 100, :length 78.53981633974483, :angle 45.0}
                      {:switch true, :length 100.0} {:length 100.0}
                      {:length 100.0}
                      {:length 100.0}
                      {:length 90.0}],
             :lanes [-10 10],
             :current-msgType "carPositions",
             :cars {"green" {:previousInPieceDistance 0.0,
                             :previousPieceIndex 0,
                             :speed 1.0
                             :previousSpeed 0,
                             :angle 0.0,
                             :lap 0,
                             :pieceIndex 0,
                             :lane {:endLaneIndex 1, :startLaneIndex 1},
                             :inPieceDistance 1.0},
                    "red" {:previousInPieceDistance 0.0,
                           :previousPieceIndex 0,
                           :speed 2.0
                           :previousSpeed 0,
                           :angle 0.0,
                           :lap 0,
                           :pieceIndex 0,
                           :lane {:endLaneIndex 0, :startLaneIndex 0},
                           :inPieceDistance 2.0}},
             :race-session {:quickRace true, :laps 3, :maxLapTimeMs 60000}})

(def tick-crash {:game-tick 0,
                 :pieces [{:length 100.0} {:length 100.0} {:length 100.0} {:switch true, :length 100.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 200, :switch true, :length 78.53981633974483, :angle 22.5} {:length 100.0} {:length 100.0} {:radius 200, :length 78.53981633974483, :angle -22.5} {:length 100.0} {:switch true, :length 100.0} {:radius 100, :length 78.53981633974483, :angle -45.0} {:radius 100, :length 78.53981633974483, :angle -45.0} {:radius 100, :length 78.53981633974483, :angle -45.0} {:radius 100, :length 78.53981633974483, :angle -45.0} {:switch true, :length 100.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 200, :length 78.53981633974483, :angle 22.5} {:radius 200, :length 78.53981633974483, :angle -22.5} {:switch true, :length 100.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:length 62.0} {:radius 100, :switch true, :length 78.53981633974483, :angle -45.0} {:radius 100, :length 78.53981633974483, :angle -45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:radius 100, :length 78.53981633974483, :angle 45.0} {:switch true, :length 100.0} {:length 100.0} {:length 100.0} {:length 100.0} {:length 90.0}],
                 :lanes [-10 10],
                 :current-msgType "carPositions",
                 :cars {"green" {:speed 1.0,
                                 :previousSpeed 0,
                                 :inPieceDistance 1.0,
                                 :previousPieceIndex 0,
                                 :previousInPieceDistance 0.0,
                                 :lane {:endLaneIndex 1, :startLaneIndex 1},
                                 :angle 0.0,
                                 :pieceIndex 0,
                                 :lap 0},
                        "red" {:speed 2.0,
                               :previousSpeed 0,
                               :inPieceDistance 2.0,
                               :previousPieceIndex 0,
                               :previousInPieceDistance 0.0,
                               :crashTick 10,
                               :lane {:endLaneIndex 0, :startLaneIndex 0},
                               :crash-count 0,
                               :crashed true,
                               :angle 0.0,
                               :pieceIndex 0,
                               :lap 0}},
                 :race-session {:quickRace true, :laps 3, :maxLapTimeMs 60000}})
